import numpy as np
from keras.utils import to_categorical

csv = './data/train_data_set.csv'
npz = './data/train_set.npz'

filenames = ['./data/dataset-0-riposo.csv', './data/dataset-1-pugno.csv', './data/dataset-2-ok.csv', './data/dataset-3-viva.csv', './data/dataset-4-corna.csv', './data/dataset-5-pistola.csv']
with open(csv, 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            outfile.write(infile.read())

x = np.loadtxt(csv, delimiter=';', usecols=list(range(0, 64)))
y = to_categorical(np.loadtxt(csv, delimiter=';', usecols=64))

print(x)
print(y)

np.savez(npz, x=x, y=y)  
